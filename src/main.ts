import Phaser, { Physics } from 'phaser';
import { scenePlayer } from './scenePlayer';
import OutlinePipelinePlugin from 'phaser3-rex-plugins/plugins/outlinepipeline-plugin'

var config : Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    pixelArt: true,
    backgroundColor: 0x886644,
    scene: [scenePlayer],
    scale: {
        parent: 'game',
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    physics: {
        default: 'matter',
        matter: {
            gravity: {
                y: 1.0
            },
            debug: true
        }
    },
    plugins: {
        global: [{
            key: 'rexOutlinePipeline',
            plugin: OutlinePipelinePlugin,
            start: true,
        }]
    }
};

var game = new Phaser.Game(config);

