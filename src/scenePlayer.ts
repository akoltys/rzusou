import Phaser from "phaser";
import OutlinePipelinePlugin from 'phaser3-rex-plugins/plugins/outlinepipeline-plugin'

enum HandSide {
    Left,
    Right
};

enum HandTween {
    Length,
    Angle
};

class HandHold {
    scene: Phaser.Scene;
    hold!: Phaser.Physics.Matter.Image;
    hands_group: number;

    constructor(scene: Phaser.Scene, hands_group: number) {
        this.scene = scene;
        this.hands_group = hands_group;
    }
}

class PlayerHand {
    scene: Phaser.Scene;
    arm!: Phaser.Physics.Matter.Image;
    forarm!: Phaser.Physics.Matter.Image;
    palm!: Phaser.Physics.Matter.Image;
    body!: Phaser.Physics.Matter.Image;
    body_group!: number;
    hand_image!: string;
    palm_image!: string;
    side!: HandSide;
    max_length!: number;
    

    constructor(scene: Phaser.Scene, side: HandSide) {
        this.scene = scene;
        this.side = side;
    }

    set_body(body: Phaser.Physics.Matter.Image, body_group: number): void {
        this.body = body;
        this.body_group = body_group;
    }

    set_image(hand_image: string, palm_image: string, x: number, y: number): void{
        this.hand_image = hand_image;
        this.palm_image = palm_image;
        this.palm = this.scene.matter.add.image(x,y,this.palm_image, undefined, { isSensor: true, isStatic: false});
        this.forarm = this.scene.matter.add.image(x,y,this.hand_image);
        this.arm = this.scene.matter.add.image(x,y,this.hand_image);
        this.max_length = this.arm.width * 3;
        this.forarm.setDisplaySize(this.max_length, this.forarm.height); 
        this.arm.setDisplaySize(this.max_length, this.arm.height); 
        this.forarm.setCollisionGroup(this.body_group);
        this.arm.setCollisionGroup(this.body_group);
    }

    set_key(key: string): void {
        var self = this;
        this.scene.input.keyboard?.on("keydown-"+key, 
            (_: any) => {
                console.log("Key", key, "is pressed!", self);
            }
        );
        this.scene.input.keyboard?.on("keyup-"+key, 
            (_: any) => {
                console.log("Key", key, "is released!", self);
        });
    }

    init(): void {
        this.scene.matter.add.joint(this.forarm.body?.gameObject, this.palm.body?.gameObject, 0, 0,
            {
                pointA: { x: -45, y: 0},
                pointB: { x: 0, y: 0},
            }
        );
        this.scene.matter.add.joint(this.forarm.body?.gameObject, this.arm.body?.gameObject, 0, 0,
            {
                pointA: { x: 50, y: -15 },
                pointB: { x: -50, y: -15 },
            }
        );
        this.scene.matter.add.joint(this.body.body?.gameObject, this.arm.body?.gameObject, 0, 0,
            {
                pointA: { x: 50, y: 50 },
                pointB: { x: 50, y: -15 },
            }
        );
    }
}



export class scenePlayer extends Phaser.Scene {
    left_hand!: PlayerHand;
    right_hand!: PlayerHand;
    body!: Phaser.Physics.Matter.Image;
    hold!: Phaser.Physics.Matter.Image;
    input_timestamp: number = 0;
    highlightedHolds: Array<any> = [];

    constructor() {
        super("Player");
    }

    preload() {
        console.log("Preload");
        this.load.image("left_hand", "assets/left_hand.png");
        this.load.image("right_hand", "assets/right_hand.png");
        this.load.image("body", "assets/body.png");
        this.load.image("hold_0", "assets/hold_0.png");
        this.load.image("palm", "assets/palm.png");
    }

    create() {
        console.log("Create");
        this.matter.world.setBounds();
        this.matter.add.mouseSpring();
        let group_body = this.matter.world.nextGroup(true);
        this.body = this.matter.add.image(250, 250, "body");
        this.body.setCollisionGroup(group_body);
        let left_hand = new PlayerHand(this, HandSide.Left);
        left_hand.set_image("left_hand", "palm", 250, 250);
        left_hand.set_body(this.body, group_body);
        left_hand.init();
        //let left_forarm = this.matter.add.image(250, 250, "left_hand");
        //left_forarm.setScale(3, 1);
        //let left_arm = this.matter.add.image(350, 250, "left_hand");
        //left_arm.setScale(3, 1);
        //let left_palm = this.matter.add.image(400, 250, "palm", undefined, { isSensor: true, isStatic: false});
        //this.matter.add.joint(left_forarm.body?.gameObject, left_palm.body?.gameObject, 0, 0,
        //    {
        //        pointA: { x: -45, y: 0},
        //        pointB: { x: 0, y: 0},
        //    }
        //);
        //this.matter.add.joint(left_forarm.body?.gameObject, left_arm.body?.gameObject, 0, 0,
        //    {
        //        pointA: { x: 50, y: -15 },
        //        pointB: { x: -50, y: -15 },
        //    }
        //);

//        let group_body = this.matter.world.nextGroup(true);
//        console.log("Body group:", group_body);
//        this.body = this.matter.add.image(250, 250, "body");
//        this.body.setCollisionGroup(group_body);
//
//        this.left_hand = new PlayerHand(this);
//        this.left_hand.set_image('left_hand', 200, 200, HandSide.Left);
//        this.left_hand.set_key("Q");
//        this.left_hand.set_body(this.body, group_body);
//        this.left_hand.init();
//
//        this.right_hand = new PlayerHand(this);
//        this.right_hand.set_image('right_hand', 300, 200, HandSide.Right);
//        this.right_hand.set_key("E");
//        this.right_hand.set_body(this.body, group_body);
//        this.right_hand.init();
//
//        this.hold = this.matter.add.image(250, 450, "hold_0");
//        this.hold.setStatic(true);
//        this.hold.setSensor(true);
//
//        var self = this;
//        this.hold.setOnCollide(function(collisionData) {
//            console.log("Collision detected!");
//            console.log("\tcollision data:", collisionData);
//            //let postFxPlugin = self.plugins.get('rexOutlinePipeline') as OutlinePipelinePlugin;
//            //let highlight = postFxPlugin.add(collisionData.bodyB.gameObject, {
//            //    thickness: 3,
//            //    outlineColor: 0x000000
//            //});
//        });
//
//        console.log("This value: ", this);
//        
//        this.hold.setOnCollideEnd(function(hold) {
//            console.log("Collision finished!", hold);
//            //let postFxPlugin = self.plugins.get('rexOutlinePipeline') as OutlinePipelinePlugin;
//            //postFxPlugin.remove(hold.bodyB.gameObject)
//        });
    }

    update() { }
}
